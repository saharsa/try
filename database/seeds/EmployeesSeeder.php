<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'bilki',
            'email' => 'bilki@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'employee',
            'created_at' =>date('Y-m-d G:i:s'),
            ],
            [
            
            'name' => 'chilki',
            'email' => 'chilki@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'employee',
            'created_at' =>date('Y-m-d G:i:s'),
            ],  
        ]);
    }
}
