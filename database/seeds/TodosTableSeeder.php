<?php

use Illuminate\Database\Seeder;
use App\Todo;
class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([[
            'title' => 'chilki',
            'created_at' =>date('Y-m-d G:i:s'),
            'user_id' => 1
            ],
            
            [
                'title' => 'bilki',
                'created_at' =>date('Y-m-d G:i:s'),
                'user_id' => 1
            ],
            [
                'title' => 'sahar',
                'created_at' =>date('Y-m-d G:i:s'),
                'user_id' => 2
            ],
        ]);}
}
