<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'sahar',
            'email' => 'sahar@gmail.com',
            'password' => '12345',
            'created_at' =>date('Y-m-d G:i:s'),
            ],
            [
            
            'name' => 'yarden',
            'email' => 'yarden@gmail.com',
            'password' => '12345',
            'created_at' =>date('Y-m-d G:i:s'),
            ],  
        ]);
    }
}
